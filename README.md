# Scanner to svg

Most of the documentation is in the notebook itself.

To gnerate a svg from any scan via png.

You need to specify the filename of the scan first.

Then you will get 3 different approchaes, choose the one, you like most.

You need to specify your dpi (dots per inch) of the scan. If you don't know it, you can use the default value of 300 dpi.

You need to specify the scaling factor for the svg. Use Inkscape, there you can directy read it from the document properties.
I still dont know, why this scaling factor is needed. But if you enter the right value, it dosent change and you can use it for all your scans.

The svg will be saved with the same name (but with .svg) in a folder svg/.